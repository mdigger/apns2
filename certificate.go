package apns

import (
	"crypto/tls"
	"encoding/asn1"
	"errors"
	"fmt"
	"io/ioutil"
	"time"

	"golang.org/x/crypto/pkcs12"
)

const appleDevIssuerCN = "Apple Worldwide Developer Relations Certification Authority"

var (
	typeCountry    = asn1.ObjectIdentifier{2, 5, 4, 6}
	typeOrgUnit    = asn1.ObjectIdentifier{2, 5, 4, 11}
	typeBundle     = asn1.ObjectIdentifier{0, 9, 2342, 19200300, 100, 1, 1}
	typeDevelopmet = asn1.ObjectIdentifier{1, 2, 840, 113635, 100, 6, 3, 1}
	typeProduction = asn1.ObjectIdentifier{1, 2, 840, 113635, 100, 6, 3, 2}
)

// Ошибки, возникающие при загрузке и разборе сертификатов.
var (
	ErrNotAppleDevCert = errors.New("not Apple Developer certificate")
	ErrExpired         = errors.New("certificate expired")
)

// Certificate описывает загруженный и разобранный сертификат и информацию о нескольких
// основных его параметрах.
type Certificate struct {
	CName      string          // полное название сертификата
	OrgUnit    string          // идентификатор организации
	Country    string          // страна
	BundleID   string          // идентификатор приложения
	Production bool            // флаг, что сертификат не для тестов
	Expire     time.Time       // до какого числа действителен
	TLS        tls.Certificate // сертификат TLS
}

// LoadCertificate загружает и разбирает сертификат в формате p12.
// Текущий код работает только с пуш-сертификатами Apple, проверяя что он подписан центром
// сертификации Apple для разработчиков.
func LoadCertificate(filename, password string) (*Certificate, error) {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	privateKey, x509Cert, err := pkcs12.Decode(data, password)
	if err != nil {
		return nil, err
	}
	if x509Cert.NotAfter.Before(time.Now()) {
		return nil, ErrExpired
	}
	if x509Cert.Issuer.CommonName != appleDevIssuerCN {
		return nil, ErrNotAppleDevCert
	}
	cert := &Certificate{
		CName:  x509Cert.Subject.CommonName,
		Expire: x509Cert.NotAfter,
		TLS: tls.Certificate{
			Certificate: [][]byte{
				x509Cert.Raw,
			},
			PrivateKey: privateKey,
		},
	}
	for _, attr := range x509Cert.Subject.Names {
		switch t := attr.Type; {
		case t.Equal(typeOrgUnit):
			cert.OrgUnit = attr.Value.(string)
		case t.Equal(typeBundle):
			cert.BundleID = attr.Value.(string)
		case t.Equal(typeCountry):
			cert.Country = attr.Value.(string)
		}
	}
	for _, attr := range x509Cert.Extensions {
		switch t := attr.Id; {
		case t.Equal(typeDevelopmet): // Development
			break
		case t.Equal(typeProduction): // Production
			cert.Production = true
			break
		}
	}
	return cert, nil
}

// String возвращает название сертификата.
func (c *Certificate) String() string {
	return c.CName
}

// Info возвращает таблицу с развернутой информацией о сертификате.
func (c *Certificate) Info() string {
	var certType = map[bool]string{
		true:  "Production",
		false: "Development",
	}
	return fmt.Sprint("Certificate:",
		"\n------------------------------------------------------------",
		"\n  Organization:        ", c.OrgUnit,
		"\n  Country:             ", c.Country,
		"\n  Bundle:              ", c.BundleID,
		"\n  Type:                ", certType[c.Production],
		"\n  Expire:              ", c.Expire.Format(time.RFC822),
		"\n------------------------------------------------------------")
}
