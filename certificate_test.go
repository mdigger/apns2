package apns

import (
	"fmt"
	"path/filepath"
	"testing"
)

const (
	password = "password"
	certPath = "pusher/*.p12"
)

func TestCertificate(t *testing.T) {
	matches, err := filepath.Glob(certPath)
	if err != nil {
		t.Fatal(err)
	}
	for _, filename := range matches {
		cert, err := LoadCertificate(filename, password)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(cert.Info())
	}
}
