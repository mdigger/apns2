package apns

import (
	"crypto/tls"
	"net"
	"sync"
	"sync/atomic"
)

// Client описывает клиента для отправки push-уведомлений через сервис APNS.
//
// Можно задать свои обработчики, которые будут вызываться сразу после установки соединения
// (OnConnect) или при получении ошибки (OnError). Последняя так же вызывается и при плановом
// закрытии соединения: в этом случае передаваемая в нее ошибка будет nil.
type Client struct {
	cert      *Certificate          // сертификат для подключения
	counter   uint32                // счетчик сообщений
	conn      *tls.Conn             // соединение с сервером
	mu        sync.RWMutex          // блокировка параллельного доступа к свойствам
	OnConnect func(*tls.Conn) error // вызывается при установке соединения
	OnError   func(error)           // вызывается при получении ошибки
}

// NewClient возвращает нового инициализированного клиента для отправки push-уведомлений через
// сервис APNS.
func NewClient(cert *Certificate) *Client {
	return &Client{cert: cert}
}

// Send отсылает уведомления на устройства с указанными токенами. Самостоятельно устанавливает
// соединение, если оно не было установлено до этого.
func (c *Client) Send(n *Notification, tokens ...[]byte) (err error) {
	ntf, err := n.withTokens(tokens...) // конвертируем во внутреннее представление
	if err != nil {
		return err
	}
	_, err = c.send(ntf)
	return err
}

// send осуществляет реальную отсылку уведомлений. Возвращает номер элемента, в котором возникла
// ошибки и саму ошибку, если есть
func (c *Client) send(ntfs ...*notification) (last int, err error) {
	// если соединение не установлено, то устанавливаем его
	if err = c.connect(); err != nil {
		return 0, err
	}
	for index, ntf := range ntfs {
		ntf.id = atomic.LoadUint32(&c.counter)                // устанавливаем уникальный идентификатор
		atomic.AddUint32(&c.counter, uint32(len(ntf.tokens))) // увеличиваем счетчик на количество токенов
		_, err = ntf.WriteTo(c.conn)                          // отправляем его на сервер
		if err != nil {
			return index, err
		}
	}
	return 0, nil
}

// IsConnected возвращает true, если соединение с сервером APNS в данный момент активно.
func (c *Client) IsConnected() bool {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.conn != nil
}

// Close закрывает ранее открытое соединение с сервером APNS.
func (c *Client) Close() (err error) {
	c.mu.Lock()
	if c.conn != nil {
		err = c.conn.Close()
		c.conn = nil
	}
	c.mu.Unlock()
	return err
}

// connect устанавливает соединение с сервером и запускает процесс работы с ним.
func (c *Client) connect() error {
	c.mu.Lock()
	defer c.mu.Unlock()
	if c.conn != nil {
		return nil
	}
	conn, err := Connect(c.cert)
	if err != nil {
		return err
	}
	// вызываем пользовательскую функцию при установке соединения, если она задана
	if c.OnConnect != nil {
		if err = c.OnConnect(conn); err != nil {
			return err
		}
	}
	c.conn = conn
	go c.reading() // запускаем процесс отслеживания ошибок
	return nil
}

// reading ожидает ответ от сервера и разбирает его.
func (c *Client) reading() (err error) {
	buffer := make([]byte, 6)    // формируем буфер для получения ответа
	_, err = c.conn.Read(buffer) // ожидаем ответа от сервера
	if err == nil {
		err = parseError(buffer) // формируем ошибку из ответа сервера
	}
	c.Close() // при любом ответе закрываем соединение
	if opError, ok := err.(*net.OpError); ok &&
		opError.Err.Error() == "use of closed network connection" {
		err = nil // сбрасываем ошибку закрытого соединения
	}
	// если определена пользовательская функция получения ошибок, то вызываем ее
	if c.OnError != nil {
		c.OnError(err)
	}
	return err
}
