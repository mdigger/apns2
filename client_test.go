package apns

import (
	"crypto/tls"
	"log"
	"math/rand"
	"net"
	"testing"
	"time"
)

var tokenStrings = []string{
	"883982D57CDC4138D71E16B5ACBCB5DEBE3E625AFCEEE809A0F32895D2EA9D51",
}

const certName = "pusher/cert.p12"

func TestClient(t *testing.T) {
	cert, err := LoadCertificate(certName, password)
	if err != nil {
		t.Fatal(err)
	}
	log.Println(cert.Info())
	ntf := &Notification{
		Payload: map[string]interface{}{
			"aps": map[string]interface{}{
				"alert": "Hello!",
				"badge": rand.Int31n(99),
			},
			"time": time.Now().Format(time.RFC3339Nano),
		}}
	tokens := TokensFromString(tokenStrings)
	addr := net.JoinHostPort(ServerAddressAndPort(cert.Production, false))
	client := NewClient(cert)
	client.OnConnect = func(conn *tls.Conn) error {
		log.Printf("Server: %s", addr)
		log.Println(TLSConnectionStateString(conn))
		return nil
	}
	client.OnError = func(err error) {
		if err != nil {
			log.Println("Connection error:", err)
		} else {
			log.Println("Connection closed")
		}
	}
	defer client.Close()
	log.Printf("Sending %d notifications", len(tokens))
	err = client.Send(ntf, tokens...)
	if err != nil {
		t.Fatal(err)
	}
}
