package apns

import (
	"crypto/tls"
	"fmt"
	"net"
	"time"
)

// TimeoutConnect содержит максимальное время ожидания ответа от сервера при соединении.
var TimeoutConnect = time.Second * 5

// Connect устанавливает соединение с сервером с использованием сертификата.
func Connect(cert *Certificate) (conn *tls.Conn, err error) {
	host, port := ServerAddressAndPort(cert.Production, false)
	return сonnect(cert, host, port)
}

// сonnect устанавливает соединение с сервером на указанный адрес и порт с использованием
// сертификата, переданного в параметрах.
func сonnect(cert *Certificate, host, port string) (conn *tls.Conn, err error) {
	addr := net.JoinHostPort(host, port)
	config := &tls.Config{
		Certificates: []tls.Certificate{cert.TLS},
		ServerName:   host,
	}
	dialer := &net.Dialer{Timeout: TimeoutConnect}
	conn, err = tls.DialWithDialer(dialer, "tcp", addr, config)
	return
}

// ServerAddressAndPort возвращает адрес и порт для соединения.
func ServerAddressAndPort(production, feedback bool) (host, port string) {
	if feedback {
		port = "2196"
		host = "feedback"
	} else {
		port = "2195"
		host = "gateway"
	}
	if !production {
		host += ".sandbox"
	}
	host += ".push.apple.com"
	return
}

// ServerAddress возвращает адрес и порт соединения в виде единой строки.
func ServerAddress(production, feedback bool) string {
	return net.JoinHostPort(ServerAddressAndPort(production, feedback))
}

// TLSConnectionStateString возвращает информацию о TLS-соединении в виде таблицы с основными
// значениями.
func TLSConnectionStateString(conn *tls.Conn) string {
	var (
		tlsVersions = map[uint16]string{
			tls.VersionSSL30: "SSL 3.0",
			tls.VersionTLS10: "TLS 1.0",
			tls.VersionTLS11: "TLS 1.1",
			tls.VersionTLS12: "TLS 1.2",
		}
		chipherSuites = map[uint16]string{
			tls.TLS_RSA_WITH_RC4_128_SHA:                "RSA with RC4 128 SHA",
			tls.TLS_RSA_WITH_3DES_EDE_CBC_SHA:           "RSA with 3DES EDE CBC SHA",
			tls.TLS_RSA_WITH_AES_128_CBC_SHA:            "RSA with AES 128 CBC SHA",
			tls.TLS_RSA_WITH_AES_256_CBC_SHA:            "RSA with AES 256 CBC SHA",
			tls.TLS_ECDHE_ECDSA_WITH_RC4_128_SHA:        "ECDHE ECDSA with RC4 128 SHA",
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_CBC_SHA:    "ECDHE ECDSA with AES 128 CBC SHA",
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_CBC_SHA:    "ECDHE ECDSA with AES 256 CBC SHA",
			tls.TLS_ECDHE_RSA_WITH_RC4_128_SHA:          "ECDHE RSA with RC4 128 SHA",
			tls.TLS_ECDHE_RSA_WITH_3DES_EDE_CBC_SHA:     "ECDHE RSA with 3DES EDE CBC SHA",
			tls.TLS_ECDHE_RSA_WITH_AES_128_CBC_SHA:      "ECDHE RSA with AES 128 CBC SHA",
			tls.TLS_ECDHE_RSA_WITH_AES_256_CBC_SHA:      "ECDHE RSA with AES 256 CBC SHA",
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256:   "ECDHE RSA with AES 128 GCM SHA256",
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256: "ECDHE ECDSA with AES 128 GCM SHA256",
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384:   "ECDHE RSA with AES 256 GCM SHA384",
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384: "ECDHE ECDSA with AES 256 GCM SHA384",
			tls.TLS_FALLBACK_SCSV:                       "FALLBACK SCSV",
		}
		yesNo = map[bool]string{
			true:  "Yes",
			false: "No",
		}
	)
	state := conn.ConnectionState()
	return fmt.Sprint("Connection state:",
		"\n------------------------------------------------------------",
		"\n  Local Address:       ", conn.LocalAddr(),
		"\n  Remote Address:      ", conn.RemoteAddr(),
		"\n  Version:             ", tlsVersions[state.Version],
		"\n  Handshake Complete:  ", yesNo[state.HandshakeComplete],
		"\n  Did Resume:          ", yesNo[state.DidResume],
		"\n  Cipher Suite:        ", chipherSuites[state.CipherSuite],
		"\n------------------------------------------------------------")
}
