package apns

import (
	"encoding/binary"
	"fmt"
)

// Error описывает формат ошибки, получаемой от сервера APNS.
type Error struct {
	Status     uint8  // код статуса
	Identifier uint32 // идентификатор уведомления с ошибкой
}

// parseError разбирает полученный от сервера ответ и генерирует на основании его описание ошибки.
func parseError(data []byte) (err *Error) {
	err = new(Error)
	if len(data) == 6 && data[0] == 8 {
		err.Status = data[1]
		err.Identifier = binary.BigEndian.Uint32(data[2:])
	}
	return
}

// Error возвращает строковое описание ошибки.
func (e Error) Error() string {
	var errors = map[uint8]string{
		0:   "No errors encountered",
		1:   "Processing error",
		2:   "Missing device token",
		3:   "Missing topic",
		4:   "Missing payload",
		5:   "Invalid token size",
		6:   "Invalid topic size",
		7:   "Invalid payload size",
		8:   "Invalid token",
		10:  "Shutdown",
		128: "Invalid frame item id", // this is not documented, but ran across it in testing
		255: "None (unknown)",
	}
	if msg, ok := errors[e.Status]; ok {
		return msg
	}
	return fmt.Sprintf("APNS error %d", e.Status)
}
