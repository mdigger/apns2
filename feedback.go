package apns

import (
	"encoding/binary"
	"encoding/hex"
	"io"
	"time"
)

// FeedbackResponse описывает формат элемента ответа от feedback сервера.
type FeedbackResponse struct {
	Timestamp uint32 // метка времени
	Token     []byte // токен устройства
}

// String возвращает строковое представление токена.
func (f *FeedbackResponse) String() string {
	return hex.EncodeToString(f.Token)
}

// Time возвращает время генерации.
func (f *FeedbackResponse) Time() time.Time {
	return time.Unix(int64(f.Timestamp), 0)
}

// Feedback осуществляет соединение с feedback сервером и возвращает список ответов от него.
// После этого соединение автоматически закрывается.
func Feedback(cert *Certificate) (result []*FeedbackResponse, err error) {
	host, port := ServerAddressAndPort(cert.Production, true)
	conn, err := сonnect(cert, host, port)
	if err != nil {
		return nil, err
	}
	result = make([]*FeedbackResponse, 0)
	header := make([]byte, 6)
	for {
		if _, err = conn.Read(header); err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}
		size := int(binary.BigEndian.Uint16(header[4:6]))
		token := make([]byte, size)
		if _, err = conn.Read(token); err != nil {
			if err == io.EOF {
				err = nil
			}
			break
		}
		response := &FeedbackResponse{
			Timestamp: binary.BigEndian.Uint32(header[0:4]),
			Token:     token,
		}
		result = append(result, response)
	}
	conn.Close()
	return
}
