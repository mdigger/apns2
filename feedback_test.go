package apns

import (
	"fmt"
	"path/filepath"
	"testing"
)

func TestFeedback(t *testing.T) {
	matches, err := filepath.Glob(certPath)
	if err != nil {
		t.Fatal(err)
	}
	for _, filename := range matches {
		cert, err := LoadCertificate(filename, password)
		if err != nil {
			t.Fatal(err)
		}
		fmt.Println(cert.CName)
		result, err := Feedback(cert)
		if err != nil {
			t.Error(err)
		}
		fmt.Printf("Response: %d items\n", len(result))
		for _, item := range result {
			fmt.Println(item.Time(), item)
		}
	}
}
