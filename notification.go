package apns

import (
	"bytes"
	"encoding/binary"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"sync"
	"time"
)

// Ошибки, возвращаемые при конвертации уведомлений во внутреннее представление и при добавлении
// уведомлений в очередь на отправку.
var (
	ErrPayloadEmpty        = errors.New("payload is empty")
	ErrPayloadTooLarge     = errors.New("payload is too large")
	ErrNotificationExpired = errors.New("notification expired")
	ErrBadTokenSize        = errors.New("bad token size")
)

// MaxPayloadSize содержит максимально допустимую длину для payload уведомления.
var MaxPayloadSize = 2048

// Notification описывает уведомление для отправки
type Notification struct {
	Payload  map[string]interface{}
	Expiry   time.Time
	Priority uint8
}

// notification описывает уведомление, готовое для отсылки в APNS, включая список токенов устройств.
type notification struct {
	tokens   [][]byte  // список токенов устройств
	payload  []byte    // содержимое сообщения
	id       uint32    // начальный уникальный идентификатор
	expiry   time.Time // время ограничения жизни
	priority uint8     // приоритет
	sended   time.Time // время отправки сообщения
}

// withTokens возвращает новое инициализированное внутренне представление уведомления для отправки,
// нормализуя исходное представление и добавляя к нему токены.
func (n *Notification) withTokens(tokens ...[]byte) (*notification, error) {
	// проверяем, что уведомление не пустое и хотя бы один токен указан
	if n == nil || len(tokens) == 0 {
		return nil, nil
	}
	// проверяем, что сообщение не пустое
	if len(n.Payload) == 0 {
		return nil, ErrPayloadEmpty
	}
	// конвертируем сообщение в формат JSON
	payloadData, err := json.Marshal(n.Payload)
	if err != nil {
		return nil, err
	}
	// проверяем на максимально допустимую длину сообщения
	if len(payloadData) > MaxPayloadSize {
		return nil, ErrPayloadTooLarge
	}
	// проверяем, что сообщение еще "не протухло"
	if !n.Expiry.IsZero() && n.Expiry.Before(time.Now().Add(time.Second)) {
		return nil, ErrNotificationExpired
	}
	// проверяем размер всех токенов
	for _, token := range tokens {
		if len(token) != TokenSize {
			return nil, ErrBadTokenSize
		}
	}
	// формируем описание уведомления
	return &notification{
		tokens:   tokens,
		payload:  payloadData,
		expiry:   n.Expiry,
		priority: n.Priority,
	}, nil
}

// WriteTo формирует и записывает в поток блок с уведомлениями для каждого токена.
func (ntf *notification) WriteTo(w io.Writer) (n int64, err error) {
	var expiry uint32 // время окончания срока действия уведомления
	if !ntf.expiry.IsZero() {
		expiry = uint32(ntf.expiry.Unix()) // переводим дату в число
	}
	// нормализуем значение приоритета до допустимых 0, 5 и 10
	var priority uint8
	if ntf.priority >= 10 {
		priority = 10 // приоритеты от 10 и выше становятся 10
	} else if ntf.priority > 0 {
		priority = 5 // приоритеты от 1 до 5 становятся 5
	}
	payloadLength := len(ntf.payload) // получаем размер основного сообщения
	size := 45 + payloadLength        // вычисляем суммарный размер блока
	if expiry > 0 {                   // если указан срок жизни, то добавляем его к размеру
		size += 7
	}
	if priority > 0 { // если указан приоритет, то добавляем его к размеру тоже
		size += 4
	}
	size *= len(ntf.tokens)                // вычисляем общий размер всех сообщений
	buf := bytesPool.Get().(*bytes.Buffer) // получаем новый буфер из пула
	defer bytesPool.Put(buf)               // освобождаем по окончании
	buf.Reset()                            // избавляемся от возможных старых данных
	if cap := size - buf.Cap(); cap > 0 {  // проверяем размер буфера
		buf.Grow(cap) // увеличиваем до необходимого размера
	}
	// вспомогательная функция для записи данных
	write := func(items ...interface{}) error {
		for _, item := range items {
			if err := binary.Write(buf, binary.BigEndian, item); err != nil {
				fmt.Println("Writing Error:", err)
				return err
			}
		}
		return nil
	}
	// TODO: разбивать на отдельные фреймы уж слишком большое количество сообщений
	err = write(uint8(2), uint32(size)) // записываем заголовок буфера
	if err != nil {
		return
	}
	for index, token := range ntf.tokens { // для всех токенов создаем единый пакет сообщений
		// токен, тело сообщения, идентификатор (увеличиваем для каждого токена)
		if err = write(uint8(1), uint16(len(token)), token,
			uint8(2), uint16(payloadLength), ntf.payload,
			uint8(3), uint16(4), ntf.id+uint32(index)); err != nil {
			return
		}
		if expiry > 0 { // срок жизни
			if err = write(uint8(4), uint16(4), expiry); err != nil {
				return
			}
		}
		if priority > 0 { // приоритет
			if err = write(uint8(5), uint16(1), priority); err != nil {
				return
			}
		}
	}
	return buf.WriteTo(w) // записываем содержимое буфера в поток
}

// bytesPool содержит пул буферов для генерации байтового представления сообщений.
var bytesPool = sync.Pool{
	New: func() interface{} {
		return new(bytes.Buffer)
	},
}
