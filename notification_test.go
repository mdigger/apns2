package apns

import (
	"bytes"
	"encoding/hex"
	"testing"
	"time"
)

const tokenstr = `F68DC4BB658CB63FD81D50826810EA8D19B437CFBF5BD33F2FD472365755B4C0`

func TestNotification(t *testing.T) {
	token, err := hex.DecodeString(tokenstr)
	if err != nil {
		t.Fatal(err)
	}
	notf := &Notification{
		Payload: map[string]interface{}{
			"aps": map[string]interface{}{
				"alert": "Hello!!!",
				"badge": 48,
			},
			"time": time.Now().Format(time.RFC3339Nano),
		},
		Expiry:   time.Now().Add(time.Minute * 10),
		Priority: 0,
	}
	ntf, err := notf.withTokens(token)

	ntf.id = 1
	var buf bytes.Buffer
	_, err = ntf.WriteTo(&buf)
	if err != nil {
		t.Fatal(err)
	}
	// fmt.Println(buf.Len(), buf.Bytes())
	if buf.Len() != 141 {
		t.Error("bad notification length")
	}
}
