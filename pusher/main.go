package main

import (
	"crypto/tls"
	"encoding/hex"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"

	"github.com/mdigger/apns2"
)

func main() {
	certFileName := flag.String("c", "cert.p12", "push `certificate`")
	password := flag.String("p", "", "certificate `password`")
	notificationFileName := flag.String("f", "", "JSON `file` with push message")
	alert := flag.String("t", "Hello!", "message `text`")
	badge := flag.Uint("b", 0, "`badge` number")
	connectionInfo := flag.Bool("s", false, "print connection state info")
	sertificateInfo := flag.Bool("i", false, "print certificate info")
	flag.Usage = func() {
		fmt.Fprint(os.Stderr, "Send Apple Push notification\n")
		fmt.Fprintf(os.Stderr, "%s [-params] <token> [<token2> [...]]\n", os.Args[0])
		flag.PrintDefaults()
		fmt.Fprintln(os.Stderr, "\n"+`Sample JSON file:
  { 
    "payload": {
      "aps": {
        "alert": "message",
        "badge": 0 
      }
    }
  }`)
	}
	flag.Parse()
	log.SetFlags(0)

	if flag.NArg() < 1 {
		log.Fatalln("Error: no tokens")
	}
	tokens := make([][]byte, flag.NArg())
	for i, token := range flag.Args() {
		btoken, err := hex.DecodeString(token)
		if err != nil || len(btoken) != 32 {
			log.Fatalln("Bad token:", token)
		}
		tokens[i] = btoken
	}
	cert, err := apns.LoadCertificate(*certFileName, *password)
	if err != nil {
		log.Fatalln("Error loading certificate:", err)
	}
	notification := new(apns.Notification)
	if *notificationFileName != "" {
		data, err := ioutil.ReadFile(*notificationFileName)
		if err != nil {
			log.Fatalln("Error loading push file:", err)
		}
		err = json.Unmarshal(data, notification)
		if err != nil {
			log.Fatalln("Error parsing push file:", err)
		}
	} else if *alert != "" {
		notification.Payload = map[string]interface{}{
			"aps": map[string]interface{}{
				"alert": *alert,
				"badge": *badge,
			},
		}
	} else {
		log.Fatalln("Nothing to send")
	}
	if *sertificateInfo {
		log.Println(cert.Info())
	}
	client := apns.NewClient(cert)
	if *connectionInfo {
		client.OnConnect = func(conn *tls.Conn) error {
			log.Println(apns.TLSConnectionStateString(conn))
			log.Printf("  Server:              %s\n"+
				"------------------------------------------------------------",
				apns.ServerAddress(cert.Production, false))
			return nil
		}
	}
	client.OnError = func(err error) {
		if err != nil {
			client.Close()
			log.Fatalln("Connection error:", err)
		} else {
			log.Println("Connection closed")
		}
	}
	log.Printf("Sending %d notification(s)...", len(tokens))
	err = client.Send(notification, tokens...)
	if err != nil {
		client.Close()
		log.Fatalln("Error sending notification:", err)
	}
	client.Close()
	log.Println("Complete!")
}
