package apns

import (
	"container/list"
	"time"
)

type queue struct {
	l *list.List
}

func (q *queue) Add(n *notification) {
	q.l.PushBack(n)
}

func (q *queue) RemoveOld(older time.Duration) {
	var next *list.Element
	for e := q.l.Front(); e != nil; e = next {
		next = e.Next()
		ntf := e.Value.(*notification)
		if time.Since(ntf.sended) > older {
			q.l.Remove(e)
		}
	}
}

func (q *queue) RemoveBefore(id uint32) {
	var next *list.Element
	for e := q.l.Front(); e != nil; e = next {
		next = e.Next()
		ntf := e.Value.(*notification)
		q.l.Remove(e)
		if ntf.id >= id && ntf.id+uint32(len(ntf.tokens)) <= id {
			return
		}
	}
}
