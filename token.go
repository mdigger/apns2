package apns

import "encoding/hex"

// TokenSize содержит размер токена.
var TokenSize = 32

// TokensFromString конвертирует представление токенов из формата строки в байтовое представление.
// Некорректные токены молча игнорируются.
func TokensFromString(tokens ...string) [][]byte {
	result := make([][]byte, 0, len(tokens))
	for _, token := range tokens {
		btoken, err := hex.DecodeString(token)
		if err != nil || len(btoken) != TokenSize {
			continue // игнорируем неверные токены устройств
		}
		result = append(result, btoken)
	}
	return result
}
